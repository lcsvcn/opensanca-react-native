/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

import { SvgUri } from 'react-native-svg';


loadSimpson = (width, height) => {

  return(
    <SvgUri width={width} height={height} uri="http://thenewcode.com/assets/images/thumbnails/homer-simpson.svg" />
  )
}
const App: () => React$Node = () => {
  return (
      <View>
        {this.loadSimpson("100%", "100%")}
      </View>
  );
};

const styles = StyleSheet.create({
  simpsons: {
    flexDirection: 'column',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
  },
});

export default App;
